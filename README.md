# How to use Cube Checker

Cube Checker allows you to quickly inspect and flag cubes to include in your analysis. Don't expect any fancy features; it's just yes or no. It prints out a copy-pasteable list of full paths to the cubes you want.

## Requirements 

- astropy, to read fits files (http://www.astropy.org/)
    - Install astropy with **pip install astropy**
- X-forwarding ssh connection, if working on a remote server

## Usage
First, check out the code from the repo.

Then from a bash terminal:
```
$ cubes=`ls /path/to/cubes/*.fits`
$ python /path/to/cubechecker/cube_checker.py ${cubes}
```
You will be presented with a figure window and a dialog in the terminal window. The dialog displays:

- i/N files
- cube filename
- Exposure time
- Seeing
- Airmass
- Keep? (Y/n)

If you want to *keep* the cube, type 'y' (or anything that starts with y or Y). 

If you want to *discard* the cube type 'n' (or anything that starts with n or N).

Once you have flagged a cube, the figure window will close and a new one, for the next cube, will appear.

Once you've finished, it will ask you if you want to loop again. This is mostly in case you messed up. If you're finished, type 'n', and it will print a list of filepaths for "good" cubes. Otherwise, type 'y' and it will start over.